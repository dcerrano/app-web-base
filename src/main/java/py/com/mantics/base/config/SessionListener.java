package py.com.mantics.base.config;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		// 1 hora de sesión inactiva
		event.getSession().setMaxInactiveInterval(60 * 60);
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {

	}
}