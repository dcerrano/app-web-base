package py.com.mantics.base.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import py.com.mantics.base.dto.UserSession;
import py.com.mantics.base.service.UsuarioService;

/**
 * Esta instancia se ejecuta en el momento que usuario inicia sesión. Se guarda
 * información de login
 *
 */
@Service
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	private final Logger logger = LoggerFactory.getLogger(LoginSuccessHandler.class);

	@Autowired
	private ApplicationContext appContext;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {

		UserSession userSession = appContext.getBean(UserSession.class);
		userSession.setUser(userSession.getUserTmp());
		loadRoles(userSession);
		try {
			UsuarioService userService = appContext.getBean(UsuarioService.class);
			userService.saveLoginInfo(request, userSession.getUser());
		} catch (Exception ex) {
			logger.error("No se pudo guardar info de login", ex);
		}
	}

	protected void loadRoles(UserSession userSession) {

		// get security context from thread local
		SecurityContext context = SecurityContextHolder.getContext();
		if (context == null) {
			return;
		}

		Authentication authentication = context.getAuthentication();
		if (authentication == null) {
			return;
		}

		for (GrantedAuthority auth : authentication.getAuthorities()) {
			userSession.addPermission(auth.getAuthority());
		}
	}

}
