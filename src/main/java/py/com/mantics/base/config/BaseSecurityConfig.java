package py.com.mantics.base.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.AuthenticationEntryPoint;

public class BaseSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

		auth.userDetailsService(userDetailsService).passwordEncoder(new Md5PasswordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expression = http
				.authorizeRequests();

		http.exceptionHandling().authenticationEntryPoint(getAuthenticationEntryPoint());

		http.requestCache().disable();
		setAntMatchers(expression);
		http.cors();
	}

	protected void setAntMatchers(
			ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expression) {
		expression.antMatchers(HttpMethod.GET, "/usuarios/**").hasAnyAuthority("Usuario_ver", "root");
		expression.antMatchers(HttpMethod.POST, "/usuarios/**").hasAnyAuthority("Usuario_guardar", "root");
		expression.antMatchers(HttpMethod.DELETE, "/usuarios/**").hasAnyAuthority("Usuario_borrar", "root");
	}

	private AuthenticationEntryPoint getAuthenticationEntryPoint() {

		return new AuthenticationEntryPoint() {

			@Override
			public void commence(HttpServletRequest request, HttpServletResponse response,
					AuthenticationException authException) throws IOException, ServletException {

				response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			}
		};
	}

}
