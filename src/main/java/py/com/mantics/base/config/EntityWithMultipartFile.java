package py.com.mantics.base.config;

import org.springframework.web.multipart.MultipartFile;

public interface EntityWithMultipartFile {

	byte[] getBytes();

	void setBytes(byte[] bytes);

	void setMultipartFile(MultipartFile multipartFile);

	MultipartFile getMultipartFile();
}
