package py.com.mantics.base.config;

import java.util.Date;

import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import py.com.mantics.base.domain.AuditRevEntity;

/**
 * Cada vez que se modifica un registro,en este listener se obtiene quién hizo
 * el cambio y cuándo.
 *
 */
public class AuditListener implements RevisionListener {

	@Override
	public void newRevision(Object revisionEntity) {

		AuditRevEntity auditRevEntity = (AuditRevEntity) revisionEntity;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		auditRevEntity.setUsername(authentication.getName());
		auditRevEntity.setCreated(new Date());
	}
}
