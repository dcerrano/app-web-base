package py.com.mantics.base.exception;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;

import py.com.mantics.base.dto.ApiResponse;
import py.com.mantics.base.dto.FieldError;
import py.com.mantics.base.util.MessageUtil;

@ControllerAdvice
public class GenericResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private MessageUtil msg;
	protected Logger logger;

	public GenericResponseEntityExceptionHandler() {
		logger = LoggerFactory.getLogger(getClass());
	}

	@ExceptionHandler(value = { DataAccessException.class })
	protected ResponseEntity<Object> handleDataAccessException(DataAccessException dac, WebRequest request) {

		logger.error("DataAccessExc: {}", dac, dac.getMessage());
		ApiResponse<Object> resp = new ApiResponse<>();

		if (dac.getCause().getClass() == ConstraintViolationException.class) {
			ConstraintViolationException cve = (ConstraintViolationException) dac.getCause();
			String error = msg.get(cve.getConstraintName());
			if (StringUtils.isBlank(error)) {
				error = cve.getSQLException().getMessage();
			}
			resp.setErrorMessage(error);
		} else {
			resp.setErrorMessage(dac.getMessage());
		}

		return new ResponseEntity<Object>(resp, new HttpHeaders(), HttpStatus.CONFLICT);
	}

	@Override
	protected ResponseEntity<Object> handleBindException(BindException bre, HttpHeaders headers, HttpStatus status,
			WebRequest request) {

		ApiResponse<Object> resp = new ApiResponse<>();
		List<FieldError> fieldErrors = new ArrayList<FieldError>();
		for (org.springframework.validation.FieldError fe : bre.getBindingResult().getFieldErrors()) {
			FieldError error = new FieldError();
			error.setDefaultMessage(getFieldErrorMessage(fe));
			error.setField(fe.getField());
			error.setObjectName(fe.getObjectName());
			error.setRejectedValue(fe.getRejectedValue());
			fieldErrors.add(error);
		}
		resp.setFieldErrors(fieldErrors);
		resp.setErrorMessage(msg.get("validation_errors"));
		resp.setData(bre.getBindingResult().getTarget());
		HttpHeaders header = new HttpHeaders();

		header.add("error", "validationErrors");
		return new ResponseEntity<Object>(resp, headers, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({ BusinessLogicException.class })
	public ResponseEntity<Object> businessLogic(Exception exc) {

		ApiResponse<Object> resp = new ApiResponse<>();
		resp.setErrorMessage(exc.getMessage());
		logger.error("BLE: {}", exc.getMessage());
		return ResponseEntity.status(HttpStatus.CONFLICT).body(resp);
	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Object> handleAnyException(Exception exc) {

		ApiResponse<Object> resp = new ApiResponse<>();
		resp.setErrorMessage(exc.getMessage());
		logger.error("handleAnyException", exc);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
	}

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
			request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
		}
		ApiResponse<Object> resp = new ApiResponse<>();
		resp.setErrorMessage(ex.getMessage());
		return new ResponseEntity<Object>(resp, headers, status);
	}

	protected String getFieldErrorMessage(org.springframework.validation.FieldError fieldError) {

		if (StringUtils.equals(fieldError.getCode(), "typeMismatch")) {
			return msg.get("typeMismatch_for_") + fieldError.getField();
		} else {

			return msg.get(fieldError.getDefaultMessage());
		}
	}

}
