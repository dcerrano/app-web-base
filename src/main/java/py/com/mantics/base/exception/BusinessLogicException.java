package py.com.mantics.base.exception;

public class BusinessLogicException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public BusinessLogicException() {
		super();
	}

	public BusinessLogicException(String message) {
		super(message);
	}

}
