package py.com.mantics.base.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.annotation.RequestScope;

import py.com.mantics.base.domain.Usuario;
import py.com.mantics.base.dto.SessionInfo;
import py.com.mantics.base.dto.UserSession;
import py.com.mantics.base.service.UsuarioService;

@Controller
@RequestScope
public class SessionController {

	@Autowired
	private UserSession userSession;
	@Autowired
	private UsuarioService userService;

	@GetMapping("session/info")
	@ResponseBody
	public SessionInfo infoSesion(HttpServletRequest request, HttpServletResponse response) {

		SessionInfo sessionInfo = new SessionInfo();
		Usuario user = userSession.getUser();

		sessionInfo.setUsuario(user);

		sessionInfo.setPermisoList(userSession.getPermissionList());

		return sessionInfo;
	}

	@GetMapping("session/userpic.png")
	public ResponseEntity<byte[]> getFoto() {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
		Usuario user = (Usuario) userSession.getObject("user");
		String filename = null;
		if (user != null) {
			filename = user.getUsername().replace(" ", "_") + ".png";
		} else {
			filename = "userpic.png";
		}

		headers.add("content-disposition", "inline; filename=" + filename);

		return new ResponseEntity<byte[]>(userService.getProfileImage(null), headers, HttpStatus.OK);
	}

}
