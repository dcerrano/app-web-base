package py.com.mantics.base.controller.list;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import py.com.mantics.base.controller.ListController;
import py.com.mantics.base.domain.Usuario;
import py.com.mantics.base.service.UsuarioService;

@RestController
@RequestScope
@RequestMapping("users")
public class UsuarioListController extends ListController<Usuario> {
	@Autowired
	private UsuarioService service;

	@Override
	public UsuarioService getService() {
		return service;
	}

	@Override
	public String[] getTableColumns() {

		return new String[] { "username", "nombres", "apellidos" };
	}

}
