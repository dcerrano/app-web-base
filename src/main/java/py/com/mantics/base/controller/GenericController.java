package py.com.mantics.base.controller;

import java.io.IOException;

import javax.validation.Valid;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import py.com.mantics.base.config.EntityWithMultipartFile;
import py.com.mantics.base.domain.GenericEntity;
import py.com.mantics.base.exception.BusinessLogicException;
import py.com.mantics.base.service.GenericService;

public abstract class GenericController<T extends GenericEntity> extends BaseController<T> {

	protected Logger logger;

	public GenericController() {
		logger = LoggerFactory.getLogger(getClass());
	}

	@Override
	public abstract GenericService<T> getService();

	@PostMapping
	@ResponseBody
	public ResponseEntity<T> create(@Valid T entity) {

		if (entity.getId() == null) {
			readEntityWithMultipartFile(entity);
			entity = getService().insert(entity);
			return new ResponseEntity<>(entity, HttpStatus.CREATED);
		} else {
			return update(entity);
		}

	}

	@PutMapping
	@ResponseBody
	public ResponseEntity<T> update(@Valid T entity) {

		readEntityWithMultipartFile(entity);
		entity = getService().update(entity);
		return new ResponseEntity<>(entity, HttpStatus.OK);
	}

	@GetMapping("{id}")
	@ResponseBody
	public ResponseEntity<T> findOne(@PathVariable Long id) {

		T entity = getService().findOne(id);
		if (entity == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok(entity);
		}
	}

	@DeleteMapping("{id}")
	@ResponseBody
	public ResponseEntity<Void> delete(@PathVariable Long id) {

		T entity = getService().findOne(id);
		if (entity == null) {
			return ResponseEntity.status(HttpStatus.GONE).build();
		} else {
			getService().delete(entity);
			return ResponseEntity.noContent().build();
		}
	}

	protected void readEntityWithMultipartFile(T entity) {

		if (!EntityWithMultipartFile.class.isAssignableFrom(entity.getClass())) {
			return;
		}
		EntityWithMultipartFile entityWithMultipart = (EntityWithMultipartFile) entity;
		MultipartFile multipartFile = entityWithMultipart.getMultipartFile();
		if (multipartFile != null && multipartFile.getSize() > 0) {
			checkMultipart(entity, multipartFile);
			entityWithMultipart.setBytes(getBytesFromMultipartFile(entityWithMultipart.getMultipartFile()));
		} else if (entity.getId() != null) {
			EntityWithMultipartFile tmp = (EntityWithMultipartFile) getService().findOne(entity.getId());
			entityWithMultipart.setBytes(tmp.getBytes());
		}
	}

	protected void checkMultipart(T entity, MultipartFile multipartFile) {
		if (!multipartFile.getContentType().contains("image") && multipartFile.getSize() > getMultipartMaxSize()) {
			throw new BusinessLogicException(msg.get("ble.multipart.max_size"));
		}
	}

	protected Long getMultipartMaxSize() {
		// Hasta 5 MB
		return NumberUtils.toLong(msg.get("multipart.max_size"), 5242880);
	}

	protected Long getImageMaxSize() {
		// Hasta 2 MB
		return NumberUtils.toLong(msg.get("image.max_size"), 2097152);
	}

	protected byte[] getBytesFromMultipartFile(MultipartFile multipartFile) {

		try {
			return multipartFile.getBytes();
		} catch (IOException e) {
			logger.error("Error en getImageFromMultipartFile: {}", e, e.getMessage());
			return null;
		}
	}
}
