package py.com.mantics.base.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import py.com.mantics.base.dto.SessionInfo;
import py.com.mantics.base.exception.BusinessLogicException;

@RestController
@RequestScope
public class LoginController {

	private final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private AuthenticationSuccessHandler loginSuccessHandler;
	@Autowired
	private SessionController sessionController;

	@PostMapping("login")
	public ResponseEntity<SessionInfo> login(HttpServletRequest request, HttpServletResponse response,
			@RequestParam String username, @RequestParam String password) throws ServletException {

		try {
			request.login(username, password);
			loginSuccessHandler.onAuthenticationSuccess(request, response,
					SecurityContextHolder.getContext().getAuthentication());
			return ResponseEntity.ok(sessionController.infoSesion(request, response));

		} catch (ServletException servletExc) {
			String error = servletExc.getMessage();
			logger.error("Error en login-> username: {}, {}", username, error);
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", error);
			headers.add("Access-Control-Expose-Headers", "error");

			if (error != null && error.contains("Bad credentials")) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).headers(headers).body(new SessionInfo());
			} else if (error != null && error.contains("already authenticated")) {
				return ResponseEntity.status(HttpStatus.CONFLICT).body(sessionController.infoSesion(request, response));
			} else {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).headers(headers).body(new SessionInfo());
			}
		} catch (BusinessLogicException ble) {
			logger.error("Error en login: {}", ble.getMessage());
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", ble.getMessage());

			return ResponseEntity.status(HttpStatus.CONFLICT).headers(headers).body(new SessionInfo());
		} catch (Exception ex) {
			logger.error("Error en login {}", ex.getMessage(), ex);
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", ex.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).headers(headers).body(new SessionInfo());
		}
	}

	@PostMapping("cerrar-sesion")
	public ResponseEntity<Void> cerrarSession(HttpServletRequest request) {

		SecurityContextHolder.clearContext();
		request.getSession().invalidate();

		return ResponseEntity.noContent().build();
	}

}
