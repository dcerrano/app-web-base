package py.com.mantics.base.controller;

import java.lang.reflect.ParameterizedType;

import javax.persistence.Entity;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import py.com.mantics.base.domain.GenericEntity;
import py.com.mantics.base.dto.UserSession;
import py.com.mantics.base.service.GenericService;
import py.com.mantics.base.util.MessageUtil;

public abstract class BaseController<M extends GenericEntity> {

	protected Logger logger;
	@Autowired
	protected UserSession userSession;
	@Autowired
	protected MessageUtil msg;
	protected String entityName;
	protected Class<?> entityClass;

	@SuppressWarnings("unchecked")
	public BaseController() {
		logger = LoggerFactory.getLogger(getClass());
		try {
			ParameterizedType superClass = (ParameterizedType) this.getClass().getGenericSuperclass();
			entityClass = (Class<M>) superClass.getActualTypeArguments()[0];
			Entity entity = getEntityClass().getAnnotation(Entity.class);
			if (entity != null) {
				if (StringUtils.isBlank(entity.name())) {
					entityName = getEntityClass().getSimpleName();
				} else {
					entityName = entity.name();
				}
			}
		} catch (Exception exc) {

			logger.error("Error al instanciar BaseController:" + exc.getMessage());
		}
	}

	public abstract GenericService<M> getService();

	/**
	 * @return nombre de la entidad manejada por el controlador. Ej: Usuario,
	 *         Permiso
	 */
	public String getEntityName() {

		return entityName;
	}

	protected Class<?> getEntityClass() {

		return entityClass;
	}

}
