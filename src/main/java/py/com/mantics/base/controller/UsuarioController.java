package py.com.mantics.base.controller;

import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import py.com.mantics.base.domain.Usuario;
import py.com.mantics.base.service.UsuarioService;
import py.com.mantics.base.util.EncryptUtils;

@RestController
@RequestScope
@RequestMapping("users")
public class UsuarioController extends GenericController<Usuario> {

	@Autowired
	private UsuarioService service;

	@Override
	public UsuarioService getService() {
		return service;
	}

	@PostMapping("changepass")
	public ResponseEntity<Void> changePass(@RequestParam Long userId, @RequestParam String password)
			throws NoSuchAlgorithmException {
		Usuario oldUser = service.findOne(userId);

		oldUser.setPassword(EncryptUtils.getMD5(password));
		service.update(oldUser);
		return ResponseEntity.ok().build();
	}
}
