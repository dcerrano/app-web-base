package py.com.mantics.base.controller;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import py.com.mantics.base.config.EntityWithMultipartFile;
import py.com.mantics.base.domain.GenericEntity;
import py.com.mantics.base.dto.DataTableModel;

public abstract class ListController<M extends GenericEntity> extends BaseController<M> {

	public static boolean WITHOUT_WHERE = false;
	public static boolean WITH_WHERE = true;
	public static int ALL_ROWS = 0;
	private String[] tableColumns;

	public String[] getTableColumns() {

		if (tableColumns == null) {
			List<String> columns = new ArrayList<>();
			try {
				Class<?> clazz = getEntityClass();
				for (Field field : clazz.getDeclaredFields()) {
					if (isTableColumn(field)) {
						columns.add(field.getName());
					}
				}
				int size = columns.size();
				tableColumns = new String[size];
				for (int i = 0; i < size; i++) {
					tableColumns[i] = columns.get(i);
				}
			} catch (Exception exc) {
				logger.error("Error al obtener columnas de tabla: {}", exc, exc.getMessage());
			}
		}
		return tableColumns;
	}

	protected boolean isTableColumn(Field field) {

		if (field.isEnumConstant() || Modifier.isStatic(field.getModifiers())
				|| Modifier.isTransient(field.getModifiers()) || field.getType() == byte[].class) {
			return false;
		}
		return true;
	}

	public String[] getReportColumns() {

		return getTableColumns();
	}

	/**
	 * Retorna las columnas de la tabla separadas por ';' Ej:
	 * id;codigo;nombre;apellido
	 */
	public String getTableColumnsStr() {

		StringBuilder cols = new StringBuilder();
		String[] columns = getTableColumns();
		int len = columns.length;
		for (int index = 0; index < len - 1; index++) {
			cols.append(columns[index]).append(";");
		}
		cols.append(columns[len - 1]);

		return cols.toString();
	}

	@GetMapping("suggest")
	@ResponseBody
	public List<?> suggest(@RequestParam(required = false, defaultValue = "0") Integer iDisplayStart,
			@RequestParam(required = false, defaultValue = "10") Integer iDisplayLength,
			@RequestParam(value = "iSortCol_0", required = false, defaultValue = "0") Integer orderColumnIndex,
			@RequestParam(required = false) String sSearch, @RequestParam Map<String, String> params, Model model) {

		iDisplayLength = getDataTableMaxLength(iDisplayLength);

		String orderBy = getOrderBy(orderColumnIndex);
		return getDataTable(iDisplayStart, iDisplayLength, orderBy, sSearch, params).getAaData();
	}

	/**
	 * @param iDisplayStart    número de fila desde el cual se mostrarán registros
	 * @param iDisplayLength   cantidad de filas que se va a retornar. Su límite se
	 *                         define en Messages.properties->iDisplayLengthMax.
	 *                         Default: 100
	 * @param sSearch          el filtro de búsqueda
	 * @param orderColumnIndex el número de columna utilizado para hacer order by
	 * @param orderDir         para indicar ordenamiento ascendente o descendente.
	 *                         Default: "asc".
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("datatable")
	@ResponseBody
	public DataTableModel<?> getDataTable(@RequestParam Map<String, String> params) throws IOException {

		String sSearch = params.get("search[value]");
		int order = NumberUtils.toInt(params.get("order[0][column]"), 0);
		String dir = params.getOrDefault("order[0][dir]", "asc");
		int start = NumberUtils.toInt(params.get("start"), 0);
		int length = NumberUtils.toInt(params.get("length"), 10);

		String orderBy = getOrderBy(order) + " " + dir;
		if (StringUtils.contains(sSearch, ":latest")) {
			sSearch = sSearch.replace(":latest", "");
			orderBy = getDefaultLatestOrderBy(orderBy);
		}

		int dataTableLength = getDataTableMaxLength(length);

		try {
			DataTableModel data = getDataTable(start, dataTableLength, orderBy, sSearch, params);
			data.success();
			return data;
		} catch (Exception exc) {
			logger.error("Error al generar dataTable: {}", exc, exc.getMessage());
			DataTableModel data = new DataTableModel();
			List<?> list = new ArrayList<>();
			data.setAaData(list);
			data.setRecordsFiltered(0L);
			data.setRecordsTotal(0L);
			data.unSuccess();
			data.setErrorMessage(exc.getMessage());
			return data;
		}

	}

	protected String getDefaultLatestOrderBy(String orderBy) {
		return "modifiedDate DESC, " + orderBy;
	}

	protected String getOrderByColumns(Integer iSortingCols, Map<String, String> params) {

		String orderBy = "";
		for (int i = 0; i < iSortingCols; i++) {
			if (NumberUtils.isNumber(params.get("iSortCol_" + i))) {
				Integer colIndex = Integer.valueOf(params.get("iSortCol_" + i));
				String dir = params.get("sSortDir_" + i);
				if (dir != null && ("asc".compareTo(dir) == 0 || "desc".compareTo(dir) == 0)) {
					orderBy = orderBy.concat(" " + getOrderBy(colIndex));
					orderBy = orderBy.concat(" ").concat(dir).concat(", ");
				}
			}
		}
		if (orderBy.length() > 0) {
			orderBy = orderBy.substring(0, orderBy.lastIndexOf(","));
		} else {
			orderBy = getDefaultOrderBy();
		}
		return orderBy;
	}

	protected String getDefaultOrderBy() {

		return getOrderBy(0).concat(" ").concat(" desc");
	}

	protected int getDataTableMaxLength(int length) {

		/*
		 * La cantidad máxima de registros a mostrar se obtiene de Messages.properties.
		 * Si no se indica, se asume 100 como máximo
		 */
		int maxLength = NumberUtils.toInt(msg.getOrDefault("dataTableMaxLength", "100"));

		return length > maxLength ? maxLength : length;
	}

	/**
	 * Método que indica las columnas por las que puede ordenar.
	 *
	 * @return lista de columnas.
	 */
	public String[] getOrderByColumns() {

		return getTableColumns();
	}

	protected String getOrderBy(Integer orderColumnIndex) {

		String[] columns = getOrderByColumns();
		String orderBy = null;
		if (orderColumnIndex >= 0 && orderColumnIndex < columns.length) {
			orderBy = columns[orderColumnIndex];
		} else {
			orderBy = columns[0];
		}

		return orderBy;
	}

	protected DataTableModel<?> getDataTable(Integer iDisplayStart, Integer iDisplayLength, String orderBy,
			String sSearch, Map<String, String> params) {

		try {
			return getService().find(iDisplayStart, iDisplayLength, sSearch, getFilterableColumns(), orderBy);
		} catch (Exception exc) {
			logger.error("error: {}", exc, exc.getMessage());
			return new DataTableModel<M>();
		}

	}

	protected String getFilterableColumns() {

		StringBuilder sb = new StringBuilder();
		for (String column : getTableColumns()) {
			if (!StringUtils.equals(column, "id")) {
				sb.append(column).append("||");
			}
		}
		sb.append("''");// para no eliminar ultimo '||' ->Ej:
						// cedula||nombre||apellido||''
		return sb.toString();
	}

	public List<Object> toList(Object... params) {

		List<Object> list = new ArrayList<>();
		for (Object param : params) {
			list.add(param);
		}
		return list;
	}

	@GetMapping("image/{recordId}.png")
	@ResponseBody
	public ResponseEntity<byte[]> getImage(@PathVariable Long recordId) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
		String filename = recordId + ".png";

		headers.add("content-disposition", "inline; filename=" + filename);

		M bean = getService().findOne(recordId);

		return new ResponseEntity<byte[]>(getImage(bean, recordId), headers, HttpStatus.OK);

	}

	protected byte[] getImage(M bean, Long recordId) {

		byte[] foto = null;
		if (bean != null && EntityWithMultipartFile.class.isAssignableFrom(bean.getClass())) {
			foto = ((EntityWithMultipartFile) bean).getBytes();
		}

		if (foto == null) {
			try {
				Resource resource = new ClassPathResource("/images/nopic.png");
				InputStream resourceInputStream = resource.getInputStream();

				foto = IOUtils.toByteArray(resourceInputStream);
			} catch (Exception exc) {
				exc.printStackTrace();
			}

		}
		return foto;
	}

}
