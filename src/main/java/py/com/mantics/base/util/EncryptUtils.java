package py.com.mantics.base.util;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

public class EncryptUtils {

	private static final Integer OXFF = 0xFF;
	private static final Integer OX100 = 0x100;

	public static String getMD5(String value) throws NoSuchAlgorithmException {

		MessageDigest messageDigest = MessageDigest.getInstance("MD5");
		byte[] array = messageDigest.digest(value.getBytes(Charset.forName("UTF-8")));
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < array.length; ++i) {
			sb.append(Integer.toHexString((array[i] & OXFF) | OX100).substring(1, 3));
		}
		return sb.toString();
	}

	public static String encode(String encoder, String password) {

		switch (encoder) {
			case "SHA-1":
				return new ShaPasswordEncoder().encodePassword(password, null);
			case "SHA-3":
				return new Sha3PasswordEncoder().encode(password);
			default:
				return new Md5PasswordEncoder().encodePassword(password, null);
		}
	}

}
