package py.com.mantics.base.util;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

import py.una.cnc.lib.db.DataSourcePool;
import py.una.cnc.lib.db.dataprovider.SQLSource;

/**
 * @author Diego Cerrano
 *
 */
@Component
public class AppContainer {

	private SQLSource sqlSource;
	private DataSourcePool dataSourcePool;

	/**
	 * Es necesario iniciar ya las conexiones al deployar la app, ya que puede
	 * tardar
	 */
	@PostConstruct
	public void init() {

		dataSourcePool = new DataSourcePool();
		dataSourcePool.init();
		sqlSource = new SQLSource();
		sqlSource.init();
	}

	@PreDestroy
	public void stop() {

		dataSourcePool.destroy();
	}

	public DataSourcePool getDataSourcePool() {

		return dataSourcePool;
	}

	public SQLSource getSqlSource() {

		return sqlSource;
	}

	public void reloadPool() {

		DataSourcePool.reloadAll();
	}

}
