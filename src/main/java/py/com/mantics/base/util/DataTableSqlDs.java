package py.com.mantics.base.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import py.una.cnc.lib.db.dataprovider.DataTableModel;
import py.una.cnc.lib.db.dataprovider.QueryBuilder;
import py.una.cnc.lib.db.dataprovider.SQLSource;
import py.una.cnc.lib.db.dataprovider.SQLToDataTable;

@Component
@Scope("session")
public class DataTableSqlDs {

	public static boolean WITHOUT_WHERE = false;
	public static boolean WITH_WHERE = true;
	public static int ALL_ROWS = 0;

	@Autowired
	private AppContainer appContainer;

	private Map<String, String> sqlMap;
	private String defaultDataSource;

	@PostConstruct
	public void init() {

		sqlMap = new HashMap<String, String>();
	}

	public String getSql(String key) {

		String sql = sqlMap.get(key);
		if (sql == null && key != null) {
			sql = getSqlSource().get(key);
			sqlMap.put(key, sql);
		}
		return sql;
	}

	public DataTableModel<?> getDataTable(String selectName, boolean withWhere, String orderBy, String filterName,
			String sSearch, Integer offset, Integer limit, Class<?> recordClass) {

		return getDataTable(selectName, withWhere, orderBy, filterName, null, sSearch, offset, limit, recordClass);
	}

	public DataTableModel<?> getDataTable(String selectName, boolean withWhere, String orderBy, String filterName,
			List<Object> fixedParams, String sSearch, Integer offset, Integer limit, Class<?> recordClass) {

		String ds = getDefaultDataSource();
		return getDataTable(ds, selectName, withWhere, orderBy, filterName, fixedParams, sSearch, offset, limit,
				recordClass);
	}

	public DataTableModel<?> getDataTable(String dataSourceName, String selectName, boolean withWhere, String orderBy,
			String filterName, List<Object> fixedParams, String sSearch, Integer offset, Integer limit,
			Class<?> recordClass) {

		return getDataTableByQuery(dataSourceName, getSql(selectName), withWhere, orderBy, getSql(filterName),
				fixedParams, sSearch, offset, limit, recordClass);
	}

	public DataTableModel<?> getDataTableByQuery(String querySelect, boolean withWhere, String orderBy,
			String queryFilter, List<Object> fixedParams, String sSearch, Integer offset, Integer limit,
			Class<?> recordClass) {

		String ds = getDefaultDataSource();
		return getDataTableByQuery(ds, querySelect, withWhere, orderBy, queryFilter, fixedParams, sSearch, offset,
				limit, recordClass);
	}

	public DataTableModel<?> getDataTableByQuery(String dataSourceName, String querySelect, boolean withWhere,
			String orderBy, String queryFilter, List<Object> fixedParams, String sSearch, Integer offset, Integer limit,
			Class<?> recordClass) {

		QueryBuilder qbuilder = new QueryBuilder();
		qbuilder.setSelectFromWhere(querySelect);
		qbuilder.setWhereAlreadyExists(withWhere);
		qbuilder.setOrderBy(orderBy);
		qbuilder.setFilterableColumn(queryFilter);
		if (fixedParams != null) {
			qbuilder.setQueryParamsVarArgs(fixedParams.toArray());
		}
		qbuilder.setsSearch(sSearch);
		qbuilder.setOffset(offset);
		qbuilder.setLimit(limit);
		qbuilder.build();
		return getDataTable(dataSourceName, recordClass, qbuilder);
	}

	public DataTableModel<?> getDataTable(String dataSourceName, Class<?> recordClass, QueryBuilder qbuilder) {

		return new SQLToDataTable<>(appContainer.getDataSourcePool()).getData(dataSourceName, recordClass, qbuilder);
	}

	protected SQLSource getSqlSource() {

		return appContainer.getSqlSource();
	}

	public String getDefaultDataSource() {
		return defaultDataSource;
	}

	public void setDefaultDataSource(String defaultDataSource) {
		this.defaultDataSource = defaultDataSource;
	}

}
