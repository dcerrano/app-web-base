package py.com.mantics.base.util;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public final class SQLUtil {

	public static PreparedStatement setearPs(PreparedStatement ps, Object... params) throws SQLException {

		if (params != null) {
			for (int i = 0; i < params.length; i++) {
				ps.setObject(i + 1, params[i]);
			}
		}
		return ps;
	}

	/**
	 *
	 * Crea una lista de parámetros a partir de parámetros anteriores + el filtro de
	 * búsqueda. Por ejemplo, si mi consulta original ya tiene el parámetro {'CNC'}
	 * y mi filtro de búsqueda es {'DOC', 'REC'}, entonces se retornará
	 * {'CNC','%DOC%','%REC%'}
	 *
	 * Ej: buscando todos los usuarios con apellidos 'GONZALEZ' en la institucion
	 * 'CNC'. (WHERE institucion.codigo='CNC' AND apellidos like '%GONZALEZ%'). CNC
	 * es un parámetro predefinido y 'GONZALEZ' el parámetro variable. -> {'CNC',
	 * '%GONZALEZ%'}
	 *
	 * @param previousParams Parámetros estáticos de una búsqueda
	 * @param searchValues   los valores ingresados para filtrar
	 */
	public static List<Object> createLikeParamList(List<Object> fixedParams, String[] searchValues) {

		List<Object> paramList = null;
		if (fixedParams != null) {
			paramList = new ArrayList<>(fixedParams);
		} else {
			paramList = new ArrayList<>();
		}

		/*
		 * en la lista de parámetros, primero agrego los parámetros pre-existentes
		 */
		for (String param : searchValues) {
			paramList.add("%" + param + "%");
		}

		return paramList;
	}

	/** Para sql JDBC no es necesario parámetro posicional ?1, ?2, ?3... */
	public static String createSqlCondition(int cantParams, String filterableColumn, String lowerOrUpper) {

		return createSqlCondition(cantParams, filterableColumn, false, 1, lowerOrUpper);
	}

	/**
	 * Crea la condición sql a partir de una columna y la cantidad de parámetros de
	 * búsqueda. Hace LOWER(columna) LIKE LOWER(?) AND... hasta completar con ? cada
	 * parámetro. Si tengo 2 parámetros y mi columna (filterableColumn) es
	 * 'nombre||apellido', entonces la consulta será: (LOWER(nombre||apellido) LIKE
	 * LOWER(?) AND LOWER(nombre||apellido) LIKE LOWER(?))
	 *
	 */
	public static String createSqlCondition(int cantParams, String filterableColumn, boolean positional,
			int positionBegin, String lowerOrUpper) {

		if (StringUtils.isBlank(filterableColumn)) {
			return "";
		}

		if (cantParams == 0) {
			return "";
		}

		StringBuilder condition = new StringBuilder(" (");

		String lower = lowerOrUpper + " (";
		String like = ") LIKE " + lowerOrUpper + "(?) AND ";
		for (int i = 1; i <= cantParams; i++) {
			if (positional) {
				like = ") LIKE " + lowerOrUpper + "(?" + positionBegin + ") AND ";
				positionBegin++;
			}
			condition.append(lower).append(filterableColumn).append(like);

		}
		// quitar el último AND
		String newCondition = condition.substring(0, condition.length() - 4);
		newCondition = newCondition.concat(")");
		return newCondition;
	}

	/** Recibe un vararg y convierte en una lista de objetos */
	public static List<Object> createListFromVarArgs(Object... params) {

		List<Object> list = new ArrayList<>();
		for (Object obj : params) {
			list.add(obj);
		}

		/*
		 * return Arrays.asList(params); es más sencillo, pero
		 * java.lang.UnsupportedOperationException cuando se quiere agregar un nuevo
		 * item l.add(1);
		 *
		 */
		return list;
	}

	public static List<String> getColumnNames(ResultSet rs) throws SQLException {

		List<String> columns = new ArrayList<>();
		for (int colNumber = 1; colNumber <= rs.getMetaData().getColumnCount(); colNumber++) {
			String column = rs.getMetaData().getColumnName(colNumber);
			columns.add(column);
		}
		return columns;
	}

	public static <T> List<T> createList(Class<T> recordClazz, ResultSet rs)
			throws SQLException, ReflectiveOperationException {

		List<T> list = new ArrayList<T>();
		List<String> columnNames = getColumnNames(rs);
		while (rs.next()) {
			T record = recordClazz.newInstance();
			for (String columnName : columnNames) {
				setObjectFieldValue(record, columnName, rs.getObject(columnName));
			}
			list.add(record);
		}
		return list;
	}

	public static <T> void setObjectFieldValue(T object, String fieldName, Object value)
			throws ReflectiveOperationException {

		try {
			Field field = object.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			field.set(object, value);
			field.setAccessible(false);
		} catch (NoSuchFieldException nsfe) {
			// no imprimir log cuando no se encuentra columna en la clase
		}
	}

}
