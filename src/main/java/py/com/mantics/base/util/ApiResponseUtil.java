package py.com.mantics.base.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.context.annotation.SessionScope;

import py.com.mantics.base.dto.ApiResponse;
import py.com.mantics.base.dto.FieldError;

@Component
@SessionScope
public class ApiResponseUtil {

	@Qualifier("mvcValidator")
	@Autowired
	private SmartValidator smartValidator;
	@Autowired
	private MessageUtil msg;

	public void processBindingResult(ApiResponse<?> response, BindingResult bindingResult, ModelMap modelMap) {

		if (bindingResult.hasErrors()) {
			response.setErrorMessage(msg.get("validation_errors"));

		}
		List<FieldError> fieldErrors = new ArrayList<FieldError>();
		for (org.springframework.validation.FieldError fe : bindingResult.getFieldErrors()) {
			FieldError error = new FieldError();
			String errorMsg = null;
			if (StringUtils.equals(fe.getCode(), "typeMismatch")) {
				errorMsg = msg.get("typeMismatch_for_") + fe.getField();
			} else {

				errorMsg = msg.get(fe.getDefaultMessage());
			}
			error.setDefaultMessage(errorMsg);
			error.setField(fe.getField());
			error.setObjectName(fe.getObjectName());
			error.setRejectedValue(fe.getRejectedValue());
			fieldErrors.add(error);
		}
		response.setFieldErrors(fieldErrors);
		modelMap.addAttribute("fieldErrors", fieldErrors);
	}

	public <M> List<ApiResponse<M>> getApiResponseList(Class<M> clazz, List<M> list, boolean validate) {

		ModelMap map = new ModelMap();
		List<ApiResponse<M>> prList = new ArrayList<>();
		for (M item : list) {
			ApiResponse<M> per = new ApiResponse<>();

			if (validate) {
				BindingResult bindingResult = new BeanPropertyBindingResult(item, "generic");
				smartValidator.validate(item, bindingResult);

				if (bindingResult.hasErrors()) {
					processBindingResult(per, bindingResult, map);
				}
			}
			per.setData(item);
			prList.add(per);
		}
		return prList;
	}
}
