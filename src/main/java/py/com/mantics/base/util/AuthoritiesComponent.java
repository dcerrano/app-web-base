package py.com.mantics.base.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import py.com.mantics.base.domain.Permiso;
import py.com.mantics.base.domain.Usuario;
import py.com.mantics.base.repository.PermisoRepo;

@Component
public class AuthoritiesComponent {

	@Autowired
	private PermisoRepo permissionRepo;

	public List<GrantedAuthority> createGrantedAuthorityList(Usuario user) {

		List<GrantedAuthority> grantedAuthorityList = new ArrayList<GrantedAuthority>();
		@SuppressWarnings("serial")
		GrantedAuthority permiso = new GrantedAuthority() {

			@Override
			public String getAuthority() {

				return "app_login";
			}
		};
		grantedAuthorityList.add(permiso);
		return grantedAuthorityList;
	}

	protected List<Permiso> getUserPermissions(Usuario user) {

		try {
			List<Permiso> permissions = null;
			if (user.getRol() != null) {
				permissions = permissionRepo.findByRolId(user.getRol().getId());
			} else {
				permissions = new ArrayList<>();
			}
			return permissions;
		} catch (Exception exc) {
			return new ArrayList<>();
		}
	}

	public List<GrantedAuthority> getAuthorities(Usuario user) {
		List<GrantedAuthority> permisos = createGrantedAuthorityList(user);
		List<Permiso> permissions = getUserPermissions(user);

		for (final Permiso permission : permissions) {
			@SuppressWarnings("serial")
			GrantedAuthority permiso = new GrantedAuthority() {

				@Override
				public String getAuthority() {

					return permission.getCodigo();
				}
			};
			permisos.add(permiso);

		}
		return permisos;
	}

}
