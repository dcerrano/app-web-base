package py.com.mantics.base.util;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class BaseUtil {
	private static String smtpHost = "smtp.gmail.com";
	private static String smtpPort = "587";
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	// unificar fecha + hora -> 25/11/2020 + 08:00 -> 25/11/2020 08:00
	public static Date unirFechaHora(Date fecha, Date hora) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(hora);

		cal.set(Calendar.HOUR_OF_DAY, cal2.get(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal2.get(Calendar.MINUTE));
		return cal.getTime();
	}

	public static void sendGmail(String remitente, String contraseña, String asunto, String mensaje,
			List<String> destinatarioList, List<String> ccList, List<String> ccoList, boolean html) throws Exception {

		Properties properties = new Properties();
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.host", smtpHost);
		properties.put("mail.smtp.port", smtpPort);

		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(remitente, contraseña);
			}
		});

		MimeMessage message = new MimeMessage(session);

		message.setFrom(new InternetAddress(remitente));

		enviarGmail(destinatarioList, message, RecipientType.TO);
		enviarGmail(ccList, message, RecipientType.CC);
		enviarGmail(ccoList, message, RecipientType.BCC);

		message.setSubject(asunto, "UTF-8");
		if (html) {
			message.setContent(mensaje, "text/html; charset=UTF-8");
		} else {
			message.setText(mensaje, "UTF-8");
		}

		Transport.send(message);
	}

	private static void enviarGmail(List<String> destinatarioList, MimeMessage message, RecipientType type)
			throws AddressException, MessagingException {
		if (destinatarioList == null) {
			return;
		}
		for (String des : destinatarioList) {
			if (isValidEmail(des)) {
				message.addRecipient(type, new InternetAddress(des));
			}
		}
	}

	public static boolean isValidEmail(String email) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		return matcher.find();
	}

}
