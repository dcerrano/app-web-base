package py.com.mantics.base.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcerrano
 */
public class AppLogger {

	private final Logger logger;

	public AppLogger(String name) {

		logger = LoggerFactory.getLogger(name);
	}

	public AppLogger(Class<?> clazz) {

		logger = LoggerFactory.getLogger(clazz);
	}

	/**
	 * Es buena práctica hacer logging de las excepciones, pero en muchos casos no
	 * es necesario. Por ejemplo cuando ocurre la excepción NoResultException, no es
	 * necesario imprimir todo el stackTrace, por lo tanto solo se imprime una info
	 */
	public void info(String msg, Throwable thrown, Object... args) {

		logger.info(msg);
	}

	public void info(String msg, Object... args) {

		logger.info(msg, args);
	}

	public void debug(String msg, Object... args) {

		logger.debug(msg, args);
	}

	public void warning(String msg, Object... args) {

		logger.warn(msg, args);
	}

	public void warning(String msg, Throwable thrown, Object... args) {

		logger.warn(msg, args);
	}

	public void error(String msg, Throwable thrown, Object... args) {

		logger.error(msg, args);
		logger.error("", thrown);
	}

	public void errorNoTrace(String msg, Throwable thrown, Object... args) {

		logger.error(msg, args);
	}

	public void error(String msg, Object... args) {

		logger.error(msg, args);
	}

}
