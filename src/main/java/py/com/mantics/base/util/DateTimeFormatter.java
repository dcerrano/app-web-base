package py.com.mantics.base.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

@Component
public class DateTimeFormatter implements Formatter<Date> {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private SimpleDateFormat simpleDateTimeFormat;

	private List<SimpleDateFormat> dateFormatList = new ArrayList<>();

	public DateTimeFormatter() {
		super();
		simpleDateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat timeFomat = new SimpleDateFormat() {

			private static final long serialVersionUID = -2480145170007940792L;

			@Override
			public Date parse(String text) {

				return new Date(Long.parseLong(text));
			}
		};
		dateFormatList.add(timeFomat);
		dateFormatList.add(simpleDateTimeFormat);
		dateFormatList.add(new SimpleDateFormat("dd/MM/yyyy"));
		dateFormatList.add(new SimpleDateFormat("HH:mm:ss"));
	}

	@Override
	public Date parse(final String text, final Locale locale) throws ParseException {
		for (SimpleDateFormat format : dateFormatList) {
			try {
				Date date = format.parse(text);
				return date;
			} catch (Exception ex) {
				logger.error("No se pudo formatear fecha {} -> {}, {}", text, ex.getMessage(), format.toPattern());
			}

		}
		return null;
	}

	@Override
	public String print(final Date object, final Locale locale) {

		final SimpleDateFormat dateFormat = simpleDateTimeFormat;
		return dateFormat.format(object);
	}

}