package py.com.mantics.base.util;

import java.io.UnsupportedEncodingException;

import org.apache.commons.lang3.StringUtils;
import org.kocakosm.pitaya.security.Digest;
import org.kocakosm.pitaya.security.Digests;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.security.crypto.password.PasswordEncoder;

public class Sha3PasswordEncoder implements PasswordEncoder {

	@Override
	public String encode(CharSequence rawPassword) {

		Digest digest = Digests.keccak512();
		String input = rawPassword.toString();
		try {
			digest.update(input.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return new String(Hex.encode(digest.digest()));
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {

		return StringUtils.equals(encode(rawPassword), encodedPassword);
	}
}
