package py.com.mantics.base.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import py.com.mantics.base.dto.DataTableModel;

@Repository
@RequestScoped
public class RepositoryUtil {

	protected static final String ENTITY = "ENTITY";
	protected Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired(required = false)
	protected EntityManager em;

	public RepositoryUtil() {
		logger = LoggerFactory.getLogger(getClass());
	}

	@SuppressWarnings("unchecked")
	public <T> DataTableModel<T> find(Class<T> entityClass, Integer firstResult, Integer maxResults, String sSearch,
			String filterableColumn, String orderBy, String whereClause, List<Object> fixedParams) {

		String entityName = null;
		Entity entity = entityClass.getAnnotation(Entity.class);

		if (StringUtils.isBlank(entity.name())) {
			entityName = entityClass.getSimpleName();
		} else {
			entityName = entity.name();
		}
		String sqlSelect = "select object(ENTITY) from ENTITY as ENTITY ";
		String sqlCount = "SELECT count(*) FROM ENTITY as ENTITY ";
		sqlSelect = sqlSelect.replace(ENTITY, entityName);
		sqlCount = sqlCount.replace(ENTITY, entityName);

		String[] sSearchArray = null;

		Query querySelect = null;
		Query queryCount = null;
		String where = null;

		if (StringUtils.isNotBlank(sSearch)) {
			sSearchArray = sSearch.trim().split(" ");
			List<Object> paramList = SQLUtil.createLikeParamList(fixedParams, sSearchArray);

			if (StringUtils.isNotBlank(whereClause)) {
				where = whereClause + " AND ";
			} else {
				where = " WHERE ";
			}
			/**
			 * Ya puedo tener un sql SELECT * FROM T WHERE a = ?1 AND b = ?2 y debe
			 * coincidir con la cantidad de fixedParams.
			 */
			int positionBegin = fixedParams == null ? 1 : fixedParams.size() + 1;
			where = where
					+ SQLUtil.createSqlCondition(sSearchArray.length, filterableColumn, true, positionBegin, "lower");
			Object[] params = paramList.toArray();
			logger.info("WHERE: {}, PARAMS: {}", where, params);
			querySelect = createQuery(sqlSelect, where + " ORDER BY " + orderBy, params);
			queryCount = createQuery(sqlCount, where, params);
		} else {
			if (StringUtils.isNotBlank(whereClause)) {
				fixedParams = fixedParams != null ? fixedParams : new ArrayList<>();
				querySelect = createQuery(sqlSelect, whereClause + " ORDER BY " + orderBy, fixedParams.toArray());
				queryCount = createQuery(sqlCount, whereClause, fixedParams.toArray());
			} else {
				querySelect = em.createQuery(sqlSelect + " ORDER BY " + orderBy);
				queryCount = em.createQuery(sqlCount);
			}
		}

		querySelect.setFirstResult(firstResult);
		querySelect.setMaxResults(maxResults);

		DataTableModel<T> dataTable = new DataTableModel<T>();

		List<T> list = querySelect.getResultList();
		dataTable.setAaData(list);

		Long size = (Long) queryCount.getSingleResult();
		dataTable.setRecordsFiltered(size);
		dataTable.setRecordsTotal(size);
		logger.info("Registros encontrados en tabla: {} ->{} (sSearch: '{}')", entityName, size, sSearch);
		return dataTable;
	}

	public <T> DataTableModel<T> find(Class<T> entityClass, Integer firstResult, Integer maxResults, String sSearch,
			String filterableColumn, String orderBy) {

		return find(entityClass, firstResult, maxResults, sSearch, filterableColumn, orderBy, null, null);
	}

	@Transactional
	protected Query createQuery(String sql, String where, Object... params) {

		String parameters = "";

		if (where != null) {
			sql = sql.concat(" ".concat(where));
		}

		Query query = em.createQuery(sql);

		if (params != null) {
			int index = 1;
			for (Object param : params) {
				query = query.setParameter(index++, param);
				parameters = parameters + param + ", ";
			}
		}
		logger.debug("Ejecutando sentencia: {}, params: [{}]", sql, parameters);

		return query;
	}

	public Long getMax(String entityName, String columnName, String where, Object... params) {

		return getMax(Long.class, 0L, entityName, columnName, where, params);
	}

	public <V> V getMax(Class<V> columnType, V valueWhenNull, String entityName, String columnName, String where,
			Object... params) {

		String sql = "SELECT MAX(" + columnName + ") FROM " + entityName + " AS " + entityName;

		Query query = createQuery(sql, where, params);
		@SuppressWarnings("unchecked")
		V max = (V) query.getSingleResult();
		if (max == null) {
			return valueWhenNull;
		}
		return max;
	}

	public BigDecimal sumBigDecimal(String entityName, String columnName, String where, Object... params) {

		return sum(BigDecimal.class, BigDecimal.ZERO, entityName, columnName, where, params);
	}

	public Long sumLong(String entityName, String columnName, String where, Object... params) {

		return sum(Long.class, 0L, entityName, columnName, where, params);

	}

	public <S> S sum(Class<S> columnType, S valueWhenNull, String entityName, String columnName, String where,
			Object... params) {

		String sql = "SELECT SUM(" + columnName + ") FROM " + entityName + " AS " + entityName;

		Query query = createQuery(sql, where, params);
		@SuppressWarnings("unchecked")
		S sum = (S) query.getSingleResult();
		if (sum == null) {
			return valueWhenNull;
		}
		return sum;
	}
}
