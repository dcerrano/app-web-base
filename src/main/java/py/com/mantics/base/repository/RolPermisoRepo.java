package py.com.mantics.base.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import py.com.mantics.base.domain.Permiso;
import py.com.mantics.base.domain.Rol;
import py.com.mantics.base.domain.RolPermiso;

public interface RolPermisoRepo extends JpaRepository<RolPermiso, Long> {

	RolPermiso findOneByRolAndPermiso(Rol rol, Permiso permiso);

	@Modifying
	void deleteByRolAndPermiso(Rol role, Permiso permiso);

}
