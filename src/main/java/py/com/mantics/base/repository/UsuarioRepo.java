package py.com.mantics.base.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import py.com.mantics.base.domain.Usuario;

public interface UsuarioRepo extends JpaRepository<Usuario, Long> {

	@Transactional
	Usuario findByUsername(String username);

	@Transactional
	Usuario findByEmailIgnoreCase(String email);

	@Transactional
	Usuario findByHash(String hash);
}
