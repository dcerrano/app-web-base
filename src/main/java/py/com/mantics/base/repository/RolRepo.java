package py.com.mantics.base.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import py.com.mantics.base.domain.Rol;

public interface RolRepo extends JpaRepository<Rol, Long> {

}
