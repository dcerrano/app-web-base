package py.com.mantics.base.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import py.com.mantics.base.domain.LoginInfo;

public interface LoginInfoRepo extends JpaRepository<LoginInfo, Long> {

}
