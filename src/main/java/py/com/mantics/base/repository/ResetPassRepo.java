package py.com.mantics.base.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import py.com.mantics.base.domain.ResetPass;

public interface ResetPassRepo extends JpaRepository<ResetPass, Long> {

	@Query(value = "SELECT * FROM ResetPass e WHERE username = ?1 AND expired = false AND createdDate IS NOT NULL ORDER BY createdDate DESC LIMIT 1", nativeQuery = true)
	ResetPass getLastReset(String username);

	@Query("SELECT e FROM ResetPass e WHERE token = ?1")
	ResetPass findByToken(String token);

}
