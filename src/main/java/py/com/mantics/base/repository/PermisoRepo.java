package py.com.mantics.base.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import py.com.mantics.base.domain.Permiso;

public interface PermisoRepo extends JpaRepository<Permiso, Long> {

	static final String sql = "SELECT per.* FROM permiso per, rolpermiso rp WHERE rp.permiso_id = per.id AND rp.rol_id = ?1";

	Permiso findByCodigo(String codigo);

	@Query(value = sql, nativeQuery = true)
	List<Permiso> findByRolId(Long rolId);
}
