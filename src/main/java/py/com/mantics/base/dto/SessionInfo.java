package py.com.mantics.base.dto;

import java.util.List;

import py.com.mantics.base.domain.Usuario;

public class SessionInfo {

	private Usuario usuario;
	private List<String> permisoList;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<String> getPermisoList() {
		return permisoList;
	}

	public void setPermisoList(List<String> permisoList) {
		this.permisoList = permisoList;
	}

}
