package py.com.mantics.base.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import py.com.mantics.base.domain.Usuario;

/**
 * Mantiene información de la sesión del usuario una vez que se haya autenticado
 *
 */
@Component
@SessionScope
public class UserSession {

	private final Map<String, Object> objects;
	private final Map<String, Boolean> permissionMap;

	private Usuario user;
	/* Se asigna al buscar usuario usuario, todavía no se verifica contraseña */
	private Usuario userTmp;

	public UserSession() {

		objects = new HashMap<String, Object>();
		permissionMap = new HashMap<>();
	}

	public Usuario getUser() {

		return user;
	}

	public void setUser(Usuario user) {

		this.user = user;
	}

	public Usuario getUserTmp() {
		return userTmp;
	}

	public void setUserTmp(Usuario userTmp) {
		this.userTmp = userTmp;
	}

	public boolean hasPermission(String permission) {

		return hasPermission(permission, false);

	}

	public boolean hasPermission(String permission, boolean ignoreRoot) {

		return permissionMap.containsKey(permission) || (permissionMap.containsKey("root") && !ignoreRoot);

	}

	public void addPermission(String permission) {

		permissionMap.put(permission, Boolean.TRUE);
	}

	public List<String> getPermissionList() {
		List<String> list = new ArrayList<>();
		for (Entry<String, Boolean> entry : permissionMap.entrySet()) {
			list.add(entry.getKey());
		}
		return list;
	}

	public boolean isActive() {

		return user != null;
	}

	public void addObject(String key, Object value) {

		objects.put(key, value);
	}

	public Object getObject(String key) {

		return objects.get(key);
	}

	public void clearPermissionMap() {
		permissionMap.clear();
	}
}
