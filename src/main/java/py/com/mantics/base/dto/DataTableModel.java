package py.com.mantics.base.dto;

import java.util.List;

public class DataTableModel<T> {

	private Long recordsTotal;
	private Long recordsFiltered;
	private List<T> aaData;
	private Boolean success;
	private String errorMessage;

	public Long getRecordsTotal() {

		return recordsTotal;
	}

	public void setRecordsTotal(Long recordsTotal) {

		this.recordsTotal = recordsTotal;
	}

	public Long getRecordsFiltered() {

		return recordsFiltered;
	}

	public void setRecordsFiltered(Long recordsFiltered) {

		this.recordsFiltered = recordsFiltered;
	}

	public List<T> getAaData() {

		return aaData;
	}

	public void setAaData(List<T> aaData) {

		this.aaData = aaData;
	}

	public Boolean getSuccess() {

		return success;
	}

	public void success() {

		this.success = true;
	}

	public void unSuccess() {

		this.success = false;
	}

	public String getErrorMessage() {

		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {

		this.errorMessage = errorMessage;
	}

}
