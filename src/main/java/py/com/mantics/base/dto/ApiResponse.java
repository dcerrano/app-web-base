package py.com.mantics.base.dto;

import java.util.List;

public class ApiResponse<T> {
	private T data;
	private String errorMessage;
	private String infoMessage;
	private String successMessage;
	private String warningMessage;
	private List<FieldError> fieldErrors;

	public T getData() {

		return data;
	}

	public void setData(T data) {

		this.data = data;
	}

	public String getErrorMessage() {

		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {

		this.errorMessage = errorMessage;
	}

	public String getInfoMessage() {

		return infoMessage;
	}

	public void setInfoMessage(String infoMessage) {

		this.infoMessage = infoMessage;
	}

	public String getSuccessMessage() {

		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {

		this.successMessage = successMessage;
	}

	public String getWarningMessage() {

		return warningMessage;
	}

	public void setWarningMessage(String warningMessage) {

		this.warningMessage = warningMessage;
	}

	public List<FieldError> getFieldErrors() {

		return fieldErrors;
	}

	public void setFieldErrors(List<FieldError> fieldErrors) {

		this.fieldErrors = fieldErrors;
	}
}
