package py.com.mantics.base.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Diego Cerrano
 *
 */
@Entity
@Audited
@Table(uniqueConstraints = @UniqueConstraint(name = "rol_uk", columnNames = { "codigo", "groupid" }))
public class Rol extends GenericEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "{rol.codigo.notBlank}")
	@Size(max = 16)
	private String codigo;

	@NotBlank(message = "{rol.codigo.notBlank}")
	@Size(max = 100)
	private String descrip;

	private Long groupId;

	public Long getGroupId() {

		return groupId;
	}

	public void setGroupId(Long groupId) {

		this.groupId = groupId;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescrip() {
		return descrip;
	}

	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}

	@Override
	public String toString() {
		return "Rol [codigo=" + codigo + ", descrip=" + descrip + "]";
	}

}
