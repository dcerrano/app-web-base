package py.com.mantics.base.domain;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

import py.com.mantics.base.config.AuditListener;

@Entity
@RevisionEntity(AuditListener.class)
public class AuditRevEntity extends DefaultRevisionEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	private String username;
	private String screenName;

	public String getScreenName() {

		return screenName;
	}

	public void setScreenName(String screenName) {

		this.screenName = screenName;
	}

	public String getUsername() {

		return username;
	}

	public void setUsername(String username) {

		this.username = username;
	}

	public Date getCreated() {

		return created;
	}

	public void setCreated(Date created) {

		this.created = created;
	}

}
