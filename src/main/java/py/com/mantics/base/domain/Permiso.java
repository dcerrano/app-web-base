package py.com.mantics.base.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Audited
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "codigo" }))
public class Permiso extends GenericEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@NotBlank
	@Size(max = 60)
	private String codigo;

	@Size(max = 100)
	private String descrip;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescrip() {
		return descrip;
	}

	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}

	@Override
	public String toString() {
		return "Permiso [codigo=" + codigo + ", descrip=" + descrip + "]";
	}

}
