package py.com.mantics.base.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "username" }, name = "usuario_username_uk"),
		@UniqueConstraint(columnNames = { "email" }, name = "usuario_email_uk") })
@Audited
public class Usuario extends GenericEntity {

	private static final long serialVersionUID = 1L;

	@NotBlank(message = "{usuario.username.notBlank}")
	@Size(max = 100, message = "{usuario.username.size}")
	private String username;

	@JsonIgnore
	private String password;

	@Email(message = "{invalid_email")
	@Size(max = 100, message = "{usuario.email.size}")
	private String email;

	@NotBlank(message = "{usuario.name.notBlank}")
	@Size(max = 64, message = "{usuario.name.size}")
	private String nombres;

	@NotBlank(message = "{usuario.surname.notBlank}")
	@Size(max = 64, message = "{usuario.surname.size}")
	private String apellidos;

	// F: femenino, M: masculino
	@Size(max = 1, message = "{usuario.name.size}")
	private String genero;

	private Boolean activo;

	@JsonIgnore
	@Lob
	@Basic(fetch = FetchType.LAZY, optional = true)
	private byte[] foto;

	@JsonIgnore
	private transient MultipartFile multipartFile;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "usuario_rol_fk"))
	private Rol rol;

	@Size(max = 32)
	@Column(updatable = false)
	private String hash;

	public String getUsername() {

		return username;
	}

	public void setUsername(String username) {

		this.username = username;
	}

	public String getPassword() {

		return password;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public void setMultipartFile(MultipartFile multipartFile) {

		this.multipartFile = multipartFile;

	}

	@JsonIgnore
	public MultipartFile getMultipartFile() {

		return multipartFile;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	@Override
	public String toString() {
		return "Usuario [username=" + username + ", email=" + email + ", nombres=" + nombres + ", apellidos="
				+ apellidos + "]";
	}

}
