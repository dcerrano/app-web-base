package py.com.mantics.base.domain;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(uniqueConstraints = @UniqueConstraint(name = "rolpermiso_uk", columnNames = { "rol_id", "permiso_id" }))
public class RolPermiso extends GenericEntity {

	private static final long serialVersionUID = 1L;
	@ManyToOne
	@NotNull(message = "{rolPermiso.rol.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "rolpermiso_rol_fk"))
	private Rol rol;

	@ManyToOne
	@NotNull(message = "{rolPermiso.permiso.notNull}")
	@JoinColumn(foreignKey = @ForeignKey(name = "rolpermiso_permiso_fk"))
	private Permiso permiso;

	public RolPermiso() {

	}

	public RolPermiso(Rol rol, Permiso permiso) {

		super();
		this.rol = rol;
		this.permiso = permiso;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Permiso getPermiso() {
		return permiso;
	}

	public void setPermiso(Permiso permiso) {
		this.permiso = permiso;
	}

	@Override
	public String toString() {
		return "RolPermiso [rol=" + rol + ", permiso=" + permiso + "]";
	}

}
