package py.com.mantics.base.domain;

public class BaseEntity extends GenericEntity {

	/**
	 * Es solo para facilitar importación, ya que existe una clase
	 * javax.ws.rs.core.GenericEntity
	 */
	private static final long serialVersionUID = 1L;

}
