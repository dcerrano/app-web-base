package py.com.mantics.base.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table
public class ResetPass extends GenericEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String token;
	@Size(max = 100)
	private String username;
	@Size(max = 100)
	private String email;
	private Boolean expired;

	public String getToken() {

		return token;
	}

	public void setToken(String token) {

		this.token = token;
	}

	public String getUsername() {

		return username;
	}

	public void setUsername(String username) {

		this.username = username;
	}

	public Boolean getExpired() {

		return expired;
	}

	public void setExpired(Boolean expired) {

		this.expired = expired;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

}
