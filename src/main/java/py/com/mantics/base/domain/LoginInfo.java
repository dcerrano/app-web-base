package py.com.mantics.base.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Audited
public class LoginInfo extends GenericEntity {

	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(as = Date.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date date;

	@Size(max = 64)
	private String ip;
	@Size(max = 100)
	private String username;
	private String appName;
	private Long userId;
	private String origin;
	private String cookie;
	private String userAgent;
	private String forwardedFor;

	public String getAppName() {

		return appName;
	}

	public void setAppName(String appName) {

		this.appName = appName;
	}

	public Date getDate() {

		return date;
	}

	public void setDate(Date date) {

		this.date = date;
	}

	public String getIp() {

		return ip;
	}

	public void setIp(String ip) {

		this.ip = ip;
	}

	public String getUsername() {

		return username;
	}

	public void setUsername(String username) {

		this.username = username;
	}

	public String getOrigin() {

		return origin;
	}

	public void setOrigin(String origin) {

		if (origin != null) {
			if (origin.length() > 255) {
				this.origin = origin.substring(0, 254);
			} else {
				this.origin = origin;
			}

		}
	}

	public String getCookie() {

		return cookie;
	}

	public void setCookie(String cookie) {

		if (cookie != null) {
			if (cookie.length() > 255) {
				this.cookie = cookie.substring(0, 254);
			} else {
				this.cookie = cookie;
			}
		}
	}

	public String getUserAgent() {

		return userAgent;
	}

	public void setUserAgent(String userAgent) {

		if (userAgent != null) {
			if (userAgent.length() > 255) {
				this.userAgent = userAgent.substring(0, 254);
			} else {
				this.userAgent = userAgent;
			}
		}
	}

	public String getForwardedFor() {

		return forwardedFor;
	}

	public void setForwardedFor(String forwardedFor) {

		if (forwardedFor != null) {
			if (forwardedFor.length() > 255) {
				this.forwardedFor = forwardedFor.substring(0, 254);
			} else {
				this.forwardedFor = forwardedFor;
			}
		}
	}

	public Long getUserId() {

		return userId;
	}

	public void setUserId(Long userId) {

		this.userId = userId;
	}

	@Override
	public String toString() {

		return "LoginInfo [date=" + date + ", ip=" + ip + ", username=" + username + "]";
	}
}
