package py.com.mantics.base.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import py.com.mantics.base.domain.LoginInfo;
import py.com.mantics.base.repository.LoginInfoRepo;
import py.com.mantics.base.service.LoginInfoService;

/**
 * @author Diego Cerrano
 * @version 1.0.0 29/06/2017
 *
 */
@Service
@RequestScope
public class LoginInfoServiceImpl extends GenericServiceImpl<LoginInfo> implements LoginInfoService {

	@Autowired
	private LoginInfoRepo loginInfoRepo;

	@Override
	public LoginInfoRepo getRepository() {

		return loginInfoRepo;
	}

}
