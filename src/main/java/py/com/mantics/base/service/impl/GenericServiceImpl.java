package py.com.mantics.base.service.impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import py.com.mantics.base.domain.GenericEntity;
import py.com.mantics.base.dto.DataTableModel;
import py.com.mantics.base.service.GenericService;
import py.com.mantics.base.util.MessageUtil;
import py.com.mantics.base.util.RepositoryUtil;

public abstract class GenericServiceImpl<T extends GenericEntity> implements GenericService<T> {

	protected Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	protected MessageUtil msg;
	@Autowired
	protected RepositoryUtil repositoryUtil;
	protected Class<T> entityClass;

	@Override
	@Transactional
	public T insert(T entity) {

		entity.setId(null);
		entity = beforeInsertOrUpdate(entity, true);
		logger.info("Creando registro: {}", entity);
		getRepository().save(entity);
		entity = afterInsertOrUpdate(entity, true);
		return entity;
	}

	@Override
	@Transactional
	public T update(T entity) {

		logger.info("Actualizando registro: {}", entity);
		entity = beforeInsertOrUpdate(entity, false);
		getRepository().save(entity);
		entity = afterInsertOrUpdate(entity, false);
		return entity;
	}

	public T beforeInsertOrUpdate(T entity, boolean isNewRecord) {

		return entity;
	}

	public T afterInsertOrUpdate(T entity, boolean isNewRecord) {
		return entity;
	}

	@Override
	@Transactional
	public void delete(T entity) {

		logger.info("Borrando registro: {}", entity);
		getRepository().delete(entity);
	}

	@Override
	@Transactional
	public T findOne(Long id) {

		logger.info("Buscando registro con id: {}", id);
		return getRepository().findOne(id);
	}

	@Override
	@Transactional
	public List<T> findAll() {

		List<T> list = getRepository().findAll();
		logger.info("Registros encontrados: " + list.size());
		return list;
	}

	@Override
	@Transactional
	public DataTableModel<T> find(Integer offset, Integer limit, String sSearch, String filterableColumn,
			String orderBy) {

		return repositoryUtil.find(getEntityClass(), offset, limit, sSearch, filterableColumn, orderBy);
	}

	@Override
	@Transactional
	public DataTableModel<T> find(Integer offset, Integer limit, String sSearch, String filterableColumn,
			String orderBy, String whereClause, List<Object> fixedParams) {

		return repositoryUtil.find(getEntityClass(), offset, limit, sSearch, filterableColumn, orderBy, whereClause,
				fixedParams);
	}

	protected abstract JpaRepository<T, Long> getRepository();

	@SuppressWarnings("unchecked")
	protected Class<T> getEntityClass() {

		if (entityClass == null) {
			ParameterizedType superClass = (ParameterizedType) this.getClass().getGenericSuperclass();
			entityClass = (Class<T>) superClass.getActualTypeArguments()[0];
		}
		return entityClass;
	}

}
