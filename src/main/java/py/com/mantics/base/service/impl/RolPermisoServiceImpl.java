package py.com.mantics.base.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import py.com.mantics.base.domain.RolPermiso;
import py.com.mantics.base.repository.RolPermisoRepo;
import py.com.mantics.base.service.RolPermisoService;

/**
 * @author Diego Cerrano
 * @version 1.0.0 29/06/2017
 *
 */
@Service
@RequestScope
public class RolPermisoServiceImpl extends GenericServiceImpl<RolPermiso> implements RolPermisoService {

	@Autowired
	private RolPermisoRepo rolePermissionRepo;

	@Override
	public RolPermisoRepo getRepository() {

		return rolePermissionRepo;
	}

}
