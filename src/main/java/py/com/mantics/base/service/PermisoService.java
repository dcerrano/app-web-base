package py.com.mantics.base.service;

import py.com.mantics.base.domain.Permiso;

public interface PermisoService extends GenericService<Permiso> {

}
