package py.com.mantics.base.service;

import py.com.mantics.base.domain.Rol;

public interface RolService extends GenericService<Rol> {

}
