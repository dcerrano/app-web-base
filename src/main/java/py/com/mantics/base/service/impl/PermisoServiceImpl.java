package py.com.mantics.base.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import py.com.mantics.base.domain.Permiso;
import py.com.mantics.base.repository.PermisoRepo;
import py.com.mantics.base.service.PermisoService;

/**
 * @author Diego Cerrano
 * @version 1.0.0 29/06/2017
 *
 */
@Service
@RequestScope
public class PermisoServiceImpl extends GenericServiceImpl<Permiso> implements PermisoService {

	@Autowired
	private PermisoRepo permissionRepo;

	@Override
	public PermisoRepo getRepository() {

		return permissionRepo;
	}

}
