package py.com.mantics.base.service;

import py.com.mantics.base.domain.LoginInfo;

public interface LoginInfoService extends GenericService<LoginInfo> {

}
