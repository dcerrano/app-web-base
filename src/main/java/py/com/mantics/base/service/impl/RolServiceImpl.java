package py.com.mantics.base.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import py.com.mantics.base.domain.Rol;
import py.com.mantics.base.repository.RolRepo;
import py.com.mantics.base.service.RolService;

@Service
@RequestScope
public class RolServiceImpl extends GenericServiceImpl<Rol> implements RolService {

	@Autowired
	private RolRepo rolRepo;

	@Override
	public RolRepo getRepository() {

		return rolRepo;
	}

}
