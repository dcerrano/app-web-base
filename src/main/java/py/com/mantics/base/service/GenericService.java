package py.com.mantics.base.service;

import java.util.List;

import py.com.mantics.base.domain.GenericEntity;
import py.com.mantics.base.dto.DataTableModel;

public interface GenericService<T extends GenericEntity> {

	T insert(T entity);

	T update(T entity);

	void delete(T entity);

	T findOne(Long id);

	List<T> findAll();

	DataTableModel<T> find(Integer offset, Integer limit, String sSearch, String filterableColumn, String orderBy);

	DataTableModel<T> find(Integer offset, Integer limit, String sSearch, String filterableColumn, String orderBy,
			String whereClause, List<Object> fixedParams);
}
