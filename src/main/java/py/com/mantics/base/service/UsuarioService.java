package py.com.mantics.base.service;

import javax.servlet.http.HttpServletRequest;

import py.com.mantics.base.domain.Usuario;

public interface UsuarioService extends GenericService<Usuario> {

	Usuario findByUsername(String userName);

	void saveLoginInfo(HttpServletRequest request, Usuario user);

	byte[] getProfileImage(Usuario usuario);

	Usuario findByHash(String hash);
}
