package py.com.mantics.base.service.impl;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import py.com.mantics.base.domain.Usuario;
import py.com.mantics.base.dto.UserSession;
import py.com.mantics.base.service.UsuarioService;
import py.com.mantics.base.util.AuthoritiesComponent;
import py.com.mantics.base.util.MessageUtil;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

	@Autowired
	private MessageUtil msg;

	@Autowired
	private ApplicationContext appContext;
	@Autowired
	private AuthoritiesComponent authoritiesComponent;

	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {

		UserSession userSession = appContext.getBean(UserSession.class);
		try {
			UsuarioService userService = appContext.getBean(UsuarioService.class);

			Usuario user = userService.findByUsername(username);

			if (user == null) {
				throw new UsernameNotFoundException(msg.get("wrong_user_or_pass"));
			}

			userSession.setUserTmp(user);
			return buildUserForAuthentication(user);
		} catch (UsernameNotFoundException ex) {
			throw ex;
		} catch (Exception exc) {
			throw new UsernameNotFoundException(msg.get("persistence.exception"));
		}
	}

	protected UserDetails buildUserForAuthentication(final Usuario user) {

		final List<GrantedAuthority> permisos = authoritiesComponent.getAuthorities(user);

		@SuppressWarnings("serial")
		UserDetails userDetail = new UserDetails() {

			@Override
			public boolean isEnabled() {

				return BooleanUtils.isNotFalse(user.getActivo());
			}

			@Override
			public boolean isCredentialsNonExpired() {

				return true;
			}

			@Override
			public boolean isAccountNonLocked() {

				return true;
			}

			@Override
			public boolean isAccountNonExpired() {

				return true;
			}

			@Override
			public String getUsername() {

				return user.getUsername();
			}

			@Override
			public String getPassword() {

				return user.getPassword();
			}

			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {

				return permisos;
			}

		};
		return userDetail;
	}
}
