package py.com.mantics.base.service.impl;

import java.io.InputStream;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.annotation.RequestScope;

import py.com.mantics.base.domain.LoginInfo;
import py.com.mantics.base.domain.Usuario;
import py.com.mantics.base.repository.UsuarioRepo;
import py.com.mantics.base.service.LoginInfoService;
import py.com.mantics.base.service.UsuarioService;

@Service
@RequestScope
public class UsuarioServiceImpl extends GenericServiceImpl<Usuario> implements UsuarioService {

	@Autowired
	private UsuarioRepo userRepo;
	@Autowired
	private LoginInfoService loginInfoService;

	@Override
	public UsuarioRepo getRepository() {

		return userRepo;
	}

	@Override
	@Transactional
	public Usuario findByUsername(String userName) {

		return userRepo.findByUsername(userName);
	}

	@Override
	@Transactional
	public Usuario findByHash(String hash) {

		return userRepo.findByHash(hash);
	}

	@Override
	public Usuario beforeInsertOrUpdate(Usuario entity, boolean isNewRecord) {
		if (isNewRecord) {
			entity.setHash(RandomStringUtils.random(32, true, true));
		} else {
			Usuario old = userRepo.findOne(entity.getId());
			entity.setHash(old.getHash());
		}
		return super.beforeInsertOrUpdate(entity, isNewRecord);
	}

	@Override
	@Transactional
	public void saveLoginInfo(HttpServletRequest request, Usuario user) {

		Date now = new Date();
		logger.info("Usuario {}-{} inició sesión desde {} - {} a las {}", user.getId(), user.getUsername(),
				request.getRemoteHost(), request.getRemoteAddr(), now);

		LoginInfo info = new LoginInfo();
		info.setCookie(request.getHeader("Cookie"));
		info.setOrigin(request.getHeader("Origin"));
		info.setAppName(msg.get("app.name"));
		info.setDate(now);
		info.setUsername(user.getUsername());
		info.setForwardedFor(request.getHeader("X-FORWARDED-FOR"));
		info.setUserAgent(request.getHeader("User-Agent"));
		info.setIp(request.getRemoteAddr());
		info.setUserId(user.getId());

		loginInfoService.insert(info);
	}

	@Override
	@Transactional
	public byte[] getProfileImage(Usuario user) {

		byte[] foto = null;
		if (user != null) {
			foto = user.getFoto();
		}
		String filename = "/images/nopic.png";

		if (foto == null && user != null) {

			if (StringUtils.equals(user.getGenero(), "F")) {
				filename = "/images/female.png";
			} else if (StringUtils.equals(user.getGenero(), "M")) {
				filename = "/images/male.png";
			}
		}

		if (foto == null) {
			try {
				Resource resource = new ClassPathResource(filename);
				InputStream resourceInputStream = resource.getInputStream();

				foto = IOUtils.toByteArray(resourceInputStream);
			} catch (Exception exc) {
				exc.printStackTrace();
			}
		}
		return foto;
	}

}
