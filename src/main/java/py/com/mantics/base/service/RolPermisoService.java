package py.com.mantics.base.service;

import py.com.mantics.base.domain.RolPermiso;

public interface RolPermisoService extends GenericService<RolPermiso> {

}
